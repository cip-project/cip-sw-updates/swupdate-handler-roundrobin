--[[

    SWUpdate Round Robin Handler Tests

    Common test helpers and definitions

    Copyright (C) 2021-2023, Siemens AG
    Author: Christian Storm <christian.storm@siemens.com>

    SPDX-License-Identifier: GPL-2.0-or-later

--]]

--luacheck: no max line length
--luacheck: no global

local assert = require("luassert")
local stub = require("luassert.stub")
local swupdate = require("swupdate")

--- Dedent a multi-line string.
--
-- The line prefixed with the least non-tab white space is
-- reset to zero indentation. Thus, the opening line of
-- the string may retain some indentation *if* there are
-- lines of less indentation terminating the string.
--
-- @param str  Multiline string indented consistently.
-- @return Unindented string.
function string.dedent(str)
    str = str:gsub(" +$", ""):gsub("^ +", "")
    local level = math.huge
    local minPrefix = ""
    local len
    for prefix in str:gmatch("\n( +)") do
        len = #prefix
        if len < level then
            level = len
            minPrefix = prefix
        end
    end
    return (str:gsub("\n" .. minPrefix, "\n"):gsub("\n$", ""))
end

-- Inspired by the *awesome* Kepler Project:
-- https://github.com/keplerproject/lua-compat-5.2
if _VERSION == "Lua 5.1" and type(jit) ~= "table" then
    local _setfenv = setfenv
    local _loadstring = loadstring
    load = function(ld, source, _, env)
        local chunk, msg = _loadstring(ld, source)
        if not chunk then
            return chunk, msg
        end
        if env ~= nil then
            _setfenv(chunk, env)
        end
        return chunk
    end
end

--- Inject Pre/Post-Handler Functions into _G for Lua 5.1.
--
-- Lua 5.1 / LuaJIT has no _ENV, hence pre/post-handler
-- functions must be injected into _G.
--
function inject_pp_funcs(sandbox)
    local inject_pre_post_handlers = {}
    if _VERSION == "Lua 5.1" then
        for item, ref in pairs(sandbox) do
            if item:sub(1, 3) == "pp_" and type(ref) == "function" then
                _G[item] = ref
                inject_pre_post_handlers[item] = true
            end
        end
    end
    return inject_pre_post_handlers
end
function eject_pp_funcs(gfuncs)
    for item, _ in pairs(gfuncs) do
        _G[item], gfuncs[item] = nil, nil
    end
end

--- Load Testee SWUpdate Lua Handler.
--
-- Load the testee Lua SWUpdate handler and expose
-- some local functions/tables to the sandbox.locals
-- table for testability.
--
-- @param sandbox  Environment table to load the testee into.
-- @return load()-returned chunk function.
function load_testee(sandbox)
    function read(file)
        local f = assert(io.open(file, "r"))
        local content = f:read("*all")
        f:close()
        return content
    end
    local chunk = load(read("swupdate_handlers_roundrobin.lua") .. "\n" .. "locals = { \
            rrconfig = rrconfig, \
            load_config = load_config, \
            parse_csv = parse_csv, \
            strip_dev_path = strip_dev_path, \
            get_dev_path = get_dev_path, \
            ROOT_DEVICE_TYPE = ROOT_DEVICE_TYPE, \
            WHEN = WHEN \
        }", "testee", "t", sandbox)
    return chunk
end

-- Show table contents in busted's error output.
assert:set_parameter("TableFormatLevel", -1)

--- Busted Matcher for a table's key-value presence.
--
-- Simplistic matcher for testing key-value pair presence
-- in a (nested) table with globally unique keys.
--
-- Usage example:
--   match.is_present({ dev="blue", type="raw"})
--
-- @param arguments  Table with expected key-value pairs.
-- @param value      Table to check for argument's presence.
-- @return A function returning true if present, false otherwise.
local function is_present(_, arguments)
    return function(value)
        assert(type(arguments[1]) == "table")
        assert(type(value) == "table")
        local function find(tab, needle)
            for k, v in pairs(tab) do
                if k == needle then
                    return v
                end
                if type(v) == "table" then
                    local result = find(v, needle)
                    if result ~= nil then
                        return result
                    end
                end
            end
        end
        for ek, ev in pairs(arguments[1]) do
            if find(value, ek) ~= ev then
                return false
            end
        end
        return true
    end
end
assert:register("matcher", "present", is_present)

--- Busted Assertion for checking a function's multiple return values.
--
-- Simplistic assertion for checking a function's multiple
-- return values. Note: Works for non-nil values only!
--
-- Usage example:
--   assert.funcret({"one", "two" }, (function() return "one", "two" end)())
--
-- @param arguments  Table with expected return values.
-- @param ...        Actual function return values.
-- @return True if return values match, false otherwise.
local function funcret(_, arguments)
    local expected = arguments[1]
    assert(type(expected) == "table")
    for i = 2, #arguments do
        if arguments[i] ~= expected[i - 1] then
            return false
        end
    end
    return true
end
assert:register("assertion", "funcret", funcret)

-- Common Stubs
stubs = {
    io_open = function(matchlist, journal)
        return stub(io, "open", function(path)
            for _, item in pairs(matchlist) do
                if string.find(path, item[1]) ~= nil or path == item[1] then
                    if type(item[2]) == "boolean" and item[2] == false then
                        return nil, "DENIED"
                    end
                    return {
                        open = function(self, _, _)
                            return self
                        end,
                        read = function(_, _)
                            return type(item[2]) == "string" and item[2] or ""
                        end,
                        write = function(_, ...)
                            if journal ~= nil then
                                for _, v in ipairs({ ... }) do
                                    journal[#journal + 1] = v
                                end
                            end
                        end,
                        flush = function() end,
                        close = function(_) end,
                    }
                end
            end
        end)
    end,
    swupdate_call_handler = function(retcode)
        return stub(swupdate, "call_handler", function(handler, image)
            if retcode ~= nil then
                return retcode, "return code override"
            end
            return 0, nil
        end)
    end,
    swupdate_get_bootenv = function(bootenv)
        return stub(swupdate, "get_bootenv", function(key)
            for _, item in pairs(bootenv) do
                if key == item[1] then
                    return item[2]
                end
            end
            return nil
        end)
    end,
    swupdate_getroot = function(root_type, root_device)
        return stub(swupdate, "getroot", function()
            return { type = root_type, value = root_device }
        end)
    end,
}

-- Common configurations
configurations = {
    raw_cmdline_rr = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
            key=root
        ]]),
    raw_bootenv_rr = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=bootenv_rr
            key=root
        ]]),
}
