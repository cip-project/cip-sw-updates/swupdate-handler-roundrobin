--[[

    SWUpdate Round Robin Handler Tests

    General Round Robin Handler Tests

    Copyright (C) 2021-2023, Siemens AG
    Author: Christian Storm <christian.storm@siemens.com>

    SPDX-License-Identifier: GPL-2.0-or-later

--]]

--luacheck: no max line length
--luacheck: no global

local busted = require("busted")
local assert = require("luassert")
local spy = require("luassert.spy")
local stub = require("luassert.stub")
local match = require("luassert.match")
require("spec")

-- =============================================================================
-- == Main Round Robin Handler Tests
-- =============================================================================
busted.describe("SWUpdate RR Handler:", function()
    busted.before_each(function()
        swupdate = require("swupdate")
        sandbox = { swupdate = swupdate }
        setmetatable(sandbox, { __index = _G })
        spy_info = spy.on(swupdate, "info")
        spy_warn = spy.on(swupdate, "warn")
        spy_error = spy.on(swupdate, "error")
        spy_set_bootenv = spy.on(swupdate, "set_bootenv")
    end)
    busted.after_each(function()
        spy_info:revert()
        spy_warn:revert()
        spy_error:revert()
        spy_set_bootenv:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Handler registered.", function()
        local stubreghandler = stub(swupdate, "register_handler")

        load_testee(sandbox)()

        assert.stub(swupdate.register_handler).was_called_with(match.is_string(), match.is_not_nil(), match.is_number())
        stubreghandler:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Load swupdate.lua interface.", function()
        package.loaded["swupdate"] = nil
        load_testee(sandbox)()
        package.loaded["swupdate"] = swupdate
    end)

    -- -----------------------------------------------------------------------------
    busted.test("strip_dev_path() cases.", function()
        local chunk = load_testee(sandbox)
        chunk()
        assert.funcret({ "/dev/sda1", "sda1" }, sandbox.locals.strip_dev_path("/dev/sda1", ""))
        assert.funcret({ "/dev/sda1", "sda1" }, sandbox.locals.strip_dev_path("/dev/sda1"))
        assert.funcret({ "/dev/sda1", "sda1" }, sandbox.locals.strip_dev_path("/dev/sda1", "bbbbbb"))

        assert.funcret({ "sda4", "sda4" }, sandbox.locals.strip_dev_path("sda4", "/dev/"))
        assert.funcret({ "/dev/sda4", "sda4" }, sandbox.locals.strip_dev_path("/dev/sda4", "/dev/"))

        assert.funcret(
            { "root=PARTUUID=fedcba98-7654-3210-cafe-5e0710000001", "fedcba98-7654-3210-cafe-5e0710000001" },
            sandbox.locals.strip_dev_path("root=PARTUUID=fedcba98-7654-3210-cafe-5e0710000001", "root=")
        )
        assert.funcret(
            { "PARTUUID=fedcba98-7654-3210-cafe-5e0710000001", "fedcba98-7654-3210-cafe-5e0710000001" },
            sandbox.locals.strip_dev_path("PARTUUID=fedcba98-7654-3210-cafe-5e0710000001", "root=")
        )
        assert.funcret(
            { "PARTUUID=fedcba98-7654-3210-cafe-5e0710000001", "fedcba98-7654-3210-cafe-5e0710000001" },
            sandbox.locals.strip_dev_path("PARTUUID=fedcba98-7654-3210-cafe-5e0710000001")
        )
        assert.funcret({
            "root=PARTUUID=fedcba98-7654-3210-cafe-5e0710000001",
            "root=PARTUUID=fedcba98-7654-3210-cafe-5e0710000001",
        }, sandbox.locals.strip_dev_path("root=PARTUUID=fedcba98-7654-3210-cafe-5e0710000001", "bbbb="))
    end)

    -- -----------------------------------------------------------------------------
    busted.test("External Configuration successfully parsed. #config #file", function()
        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/etc/config", configurations.raw_cmdline_rr },
        })
        chunk()
        sandbox.locals.rrconfig.file = "/etc/config"

        assert.are_same(sandbox.locals.load_config(sandbox.locals.rrconfig), {
            image = {
                chainhandler = "raw",
                selector = {
                    key = "root",
                    method = "cmdline_rr",
                },
            },
        })

        iostub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Internal Configuration successfully parsed. #config", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        load_testee(sandbox)()

        assert.are_same(sandbox.locals.load_config(sandbox.locals.rrconfig), {
            image = {
                chainhandler = "raw",
                selector = {
                    key = "root",
                    method = "cmdline_rr",
                },
            },
        })
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Syntax error in configuration. #config #syntax", function()
        sandbox.configuration = string.dedent([[
            [dummy]
            key:value
        ]])

        load_testee(sandbox)()

        assert.is_equal(sandbox.handler_roundrobin({ properties = { subtype = "dummy" } }), 1)
        assert.spy(spy_warn).was_called_with(match.is_string(), match.is_string(), "key:value")
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Syntax error key not in section. #config #syntax", function()
        sandbox.configuration = string.dedent([[
        keywosection=value

        [dummy]
        keyinsection=value
        ]])

        load_testee(sandbox)()

        assert.is_equal(sandbox.handler_roundrobin({ properties = { subtype = "dummy" } }), 1)
        assert.spy(spy_error).was_called_with(match.is_string(), match.is_string())
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Absent configuration. #config", function()
        load_testee(sandbox)()
        sandbox.locals.rrconfig.file = "/dev/nonexistent_file"

        assert.is_nil(sandbox.locals.load_config(sandbox.locals.rrconfig))
        assert.is_equal(sandbox.handler_roundrobin({}), 1)
    end)

    -- -----------------------------------------------------------------------------
    busted.test("No [section] found. #config #syntax", function()
        sandbox.configuration = ""

        load_testee(sandbox)()

        assert.is_nil(sandbox.locals.load_config(sandbox.locals.rrconfig))
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Section [image] absent. #config #syntax", function()
        sandbox.configuration = string.dedent([[
            [dummy]
        ]])

        load_testee(sandbox)()

        assert.is_equal(sandbox.handler_roundrobin({ properties = { subtype = "image" } }), 1)
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Section [image.selector] absent. #config #syntax", function()
        sandbox.configuration = string.dedent([[
            [image]
        ]])

        load_testee(sandbox)()

        assert.is_equal(sandbox.handler_roundrobin({ properties = { subtype = "image" } }), 1)
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Unspecified chain-handler. #config #syntax", function()
        sandbox.configuration = string.dedent([[
            [image]

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        load_testee(sandbox)()

        assert.is_equal(sandbox.handler_roundrobin({ properties = { subtype = "image" } }), 1)
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Unavailable chain-handler. #config", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=dummy

            [image.selector]
            method=cmdline_rr
            key=dummy
        ]])

        load_testee(sandbox)()

        assert.is_equal(sandbox.handler_roundrobin({ properties = { subtype = "image" } }), 1)
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Missing 'subtype' configuration selection. #config", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({ { "/proc/cmdline", "root=/dev/sda4 rw rootwait initrd=initrd.img" } })
        chunk()

        assert.is_equal(sandbox.handler_roundrobin({ device = "/dev/single", properties = {} }), 1)

        iostub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Not implemented selector method. #config #syntax", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=nonexisting
            key=dummy
        ]])

        load_testee(sandbox)()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            1
        )
        assert.spy(spy_error).was_called_with(match.is_string(), match.is_string(), "nonexisting", "image")
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Unspecified selector key. #config #syntax", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
        ]])

        load_testee(sandbox)()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            1
        )
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Round robin list too short. #rr", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({ { "/proc/cmdline", "root=/dev/sda4 rw rootwait initrd=initrd.img" } })
        chunk()

        assert.is_equal(sandbox.handler_roundrobin({ device = "/dev/single", properties = { subtype = "image" } }), 1)
        assert.spy(spy_error).was_called_with(match.is_string(), match.is_string(), 2)

        iostub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Round robin list w/o current root. #rr", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({ { "/proc/cmdline", "root=/dev/sda4 rw rootwait initrd=initrd.img" } })
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            1
        )
        assert.spy(spy_error).was_called_with(match.is_string(), match.is_string(), "sda4", "blue green")

        iostub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Round robin target list with varying lengths, root=/dev/sda2. #rr", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/sda2 rw rootwait initrd=initrd.img" },
            { "^/dev/disk/by%-partuuid/%x%x%x%x%x%x%x%x%-", false },
            { "/dev/sd", true },
        })
        chunk()
        local callstub = stubs.swupdate_call_handler()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/sda1,/dev/sda2", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda1" }))

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/sda1,/dev/sda2,/dev/sda3", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda3" }))

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "/dev/sda1,/dev/sda2,/dev/sda3,/dev/sda4",
                properties = { subtype = "image" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda3" }))

        iostub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Round robin target list with varying lengths, root=/dev/sda3. #rr", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/sda3 rw rootwait initrd=initrd.img" },
            { "^/dev/disk/by%-partuuid/%x%x%x%x%x%x%x%x%-", false },
            { "/dev/sd", true },
        })
        chunk()
        local callstub = stubs.swupdate_call_handler()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/sda1,/dev/sda2,/dev/sda3", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda1" }))

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "/dev/sda1,/dev/sda2,/dev/sda3,/dev/sda4",
                properties = { subtype = "image" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda4" }))

        iostub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Fixed Selector, case one. #selector", function()
        sandbox.configuration = string.dedent([[
            [rootfw]
            chainhandler=raw

            [rootfw.selector]
            method=fixed
            value=/dev/mmcblk0p1
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/mmcblk0p", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "mmcblk0p3,mmcblk0p6", properties = { subtype = "rootfw" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/mmcblk0p1" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Fixed Selector, case two. #selector", function()
        sandbox.configuration = string.dedent([[
            [rootfw]
            chainhandler=raw

            [rootfw.selector]
            method=fixed
            value=mmcblk0p5
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/mmcblk0p", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(sandbox.handler_roundrobin({ device = "dummy", properties = { subtype = "rootfw" } }), 0)
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/mmcblk0p5" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Unmatched selector key. #config #selector #cmdline", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
            key=rootwait
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({ { "/proc/cmdline", "root=/dev/blue rw rootwait initrd=initrd.img" } })
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            1
        )
        assert.spy(spy_error).was_called_with(match.is_string(), match.is_string(), "rootwait")

        iostub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Generic selector key (not root=). #config #selector #cmdline", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
            key=somevalue
        ]])
        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/sda4 rw somevalue=43 rootwait initrd=initrd.img" },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/43,/dev/green", properties = { subtype = "image" } }),
            0
        )

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Selector 'getroot' fails. #selector", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=getroot_id
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            1
        )
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Selector 'getroot_id'. #selector", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=getroot_id
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local getrootstub = stubs.swupdate_getroot(swupdate.ROOT_DEVICE.PATH, "/dev/blue")
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/blue" }))

        iostub:revert()
        getrootstub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Selector 'getroot_rr' + PATH. #selector", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=getroot_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local getrootstub = stubs.swupdate_getroot(swupdate.ROOT_DEVICE.PATH, "green")
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/blue" }))

        iostub:revert()
        getrootstub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Selector 'getroot_rr' + PARTUUID. #selector", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=getroot_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=PARTUUID=fedcba98-7654-3210-cafe-5e0710000001 rw rootwait initrd=initrd.img" },
            { "^/dev/disk/by%-partuuid/%x%x%x%x%x%x%x%x%-", true },
        })
        local getrootstub =
            stubs.swupdate_getroot(swupdate.ROOT_DEVICE.PARTUUID, "PARTUUID=fedcba98-7654-3210-cafe-5e0710000001")
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "fedcba98-7654-3210-cafe-5e0710000001,fedcba98-7654-3210-cafe-5e0710000002",
                properties = { subtype = "image" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("raw", match.is_present({ device = "/dev/disk/by-partuuid/fedcba98-7654-3210-cafe-5e0710000002" }))

        iostub:revert()
        getrootstub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Selector 'cmdline_id'. #selector #cmdline", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_id
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/blue rw rootwait initrd=initrd.img" },
            { "^/dev/disk/by%-partuuid/%x%x%x%x%x%x%x%x%-", false },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/blue" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Selector 'bootenv_id'. #selector #bootenv", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=bootenv_id
            key=rootoo
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local bootenvstub = stubs.swupdate_get_bootenv({
            { "rootoo", "blue" },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/blue" }))

        iostub:revert()
        bootenvstub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("/proc/cmdline cannot be read. #selector #cmdline", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", false },
        })
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            1
        )
        assert.spy(spy_error).was_called_with(match.is_string(), match.is_string(), "/proc/cmdline")

        iostub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Selector mapping, root=/dev/green. #selector #cmdline #mapping", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "^/dev/disk/by%-partuuid/%x%x%x%x%x%x%x%x%-", false },
            { "/dev/sd", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "green->sda1,blue->sda2", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda2" }))

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green->/dev/sda1,blue->/dev/sda2",
                properties = { subtype = "image" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda2" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Selector mapping, root=/dev/blue. #selector #cmdline #mapping", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/blue rw rootwait initrd=initrd.img" },
            { "^/dev/disk/by%-partuuid/%x%x%x%x%x%x%x%x%-", false },
            { "/dev/sd", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "green->sda1,blue->sda2", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda1" }))

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green->/dev/sda1,blue->/dev/sda2",
                properties = { subtype = "image" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda1" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Selector mapping syntax error, root=/dev/green. #config #selector #cmdline #mapping", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "^/dev/disk/by%-partuuid/%x%x%x%x%x%x%x%x%-", false },
            { "/dev/sd", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green-/dev/sda1,blue->/dev/sda2",
                properties = { subtype = "image" },
            }),
            1
        )
        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green/dev/sda1,blue->/dev/sda2",
                properties = { subtype = "image" },
            }),
            1
        )
        assert.is_equal(
            sandbox.handler_roundrobin({ device = "green,blue->/dev/sda2", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda2" }))
        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,/dev/sda1,blue-/dev/sda2",
                properties = { subtype = "image" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/sda1" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Selector mapping syntax error, root=/dev/blue. #config #selector #cmdline #mapping", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/blue rw rootwait initrd=initrd.img" },
            { "^/dev/disk/by%-partuuid/%x%x%x%x%x%x%x%x%-", false },
            { "/dev/sd", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green-/dev/sda1,blue->/dev/sda2",
                properties = { subtype = "image" },
            }),
            0
        )
        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green/dev/sda1,blue->/dev/sda2",
                properties = { subtype = "image" },
            }),
            0
        )
        assert.is_equal(
            sandbox.handler_roundrobin({ device = "green,blue->/dev/sda2", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/green" }))
        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,/dev/sda1,blue-/dev/sda2",
                properties = { subtype = "image" },
            }),
            1
        )

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Chain-handler override. #config #chainhandler", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/blue rw rootwait initrd=initrd.img" },
            { "^/dev/disk/by%-partuuid/%x%x%x%x%x%x%x%x%-", false },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "/dev/blue,/dev/green",
                properties = { chainhandler = "rawfile", subtype = "image" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("rawfile", match.is_present({ device = "/dev/green", chainhandler = "rawfile" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Round robin with bootenv. #config #selector #bootenv", function()
        sandbox.configuration = configurations.raw_bootenv_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local bootenvstub = stubs.swupdate_get_bootenv({
            { "root", "blue" },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/green" }))

        iostub:revert()
        bootenvstub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Round robin with absent bootenv key. #config #selector #bootenv", function()
        sandbox.configuration = configurations.raw_bootenv_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local bootenvstub = stubs.swupdate_get_bootenv({
            { "rut", "blue" },
        })
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            1
        )

        iostub:revert()
        bootenvstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("rrtarget substitution. #config #selector #bootenv", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw
            configsection=works

            [image.selector]
            method=bootenv_rr
            key=root

            [image.bootenv]
            rrindex=${rrindex}
            rrtarget=${rrtarget}
            rrdevice=${rrdevice}
            rrparttype=${rrparttype}
            rrpartprefix=${rrpartprefix}
            rrimagedevice=${device}
            rrimagepropertiessubtype=${subtype}
            rrconfigsection=${configsection}
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local bootenvstub = stubs.swupdate_get_bootenv({
            { "root", "blue" },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/green" }))
        assert.spy(spy_set_bootenv).was_called_with("rrindex", "1")
        assert.spy(spy_set_bootenv).was_called_with("rrdevice", "/dev/green")
        assert.spy(spy_set_bootenv).was_called_with("rrparttype", tostring(sandbox.locals.ROOT_DEVICE_TYPE.PATH))
        assert.spy(spy_set_bootenv).was_called_with("rrpartprefix", "/dev/")
        assert.spy(spy_set_bootenv).was_called_with("rrtarget", "green")
        -- Note: The @param image's non-table fields and image.properties non-table fields are accessible here.
        assert.spy(spy_set_bootenv).was_called_with("rrimagedevice", "/dev/green")
        assert.spy(spy_set_bootenv).was_called_with("rrimagepropertiessubtype", "image")
        -- Note: The [image] section's keys (as referenced to by `subtype = "image"`) are accessible here.
        assert.spy(spy_set_bootenv).was_called_with("rrconfigsection", "works")

        iostub:revert()
        bootenvstub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("rrtarget substitution failure. #config #selector #bootenv", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
            key=root

            [image.bootenv]
            rrindex=${ABSENT}
            rrimagepropertiessubtype=${NO.SUBTABLE.ACCESS}
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/blue rw rootwait initrd=initrd.img" },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local bootenvstub = stubs.swupdate_get_bootenv({
            { "root", "blue" },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            0
        )
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/green" }))
        assert
            .spy(spy_error)
            .was_called_with(match.is_string(), match.is_string(), "rrimagepropertiessubtype", "${NO.SUBTABLE.ACCESS}")
        assert.spy(spy_error).was_called_with(match.is_string(), match.is_string(), "rrindex", "${ABSENT}")

        iostub:revert()
        bootenvstub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Get target device path fails.", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/blue rw rootwait initrd=initrd.img" },
        })
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "/dev/blue,/dev/green", properties = { subtype = "image" } }),
            1
        )
        assert.spy(spy_error).was_called_with(match.is_string(), match.is_string(), "green")

        iostub:revert()
    end)
end)

-- =============================================================================
-- == EFI Boot Guard Tests
-- =============================================================================
busted.describe("EFI Boot Guard:", function()
    busted.before_each(function()
        swupdate = require("swupdate")
        sandbox = { swupdate = swupdate }
        setmetatable(sandbox, { __index = _G })
        spy_set_bootenv = spy.on(swupdate, "set_bootenv")
    end)
    busted.after_each(function()
        spy_set_bootenv:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("EFI Boot Guard. #ebg", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
            key=root

            [image.bootenv]
            kernelparams=root=${rrdevice} ${cmdline_root}
            ustate=1

            [kernel]
            chainhandler=rawfile

            [kernel.selector]
            method=cmdline_rrmap
            key=root

            [kernel.bootenv]
            kernelfile=C:BOOT${rrindex}:vmlinuz
        ]])

        local cmdline = "rw rootwait initrd=initrd.img"
        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green " .. cmdline },
            { "/dev/sd", true },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(sandbox.handler_roundrobin({ device = "green,blue", properties = { subtype = "image" } }), 0)
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/blue" }))

        assert.is_equal(
            sandbox.handler_roundrobin({
                filename = "vmlinuz",
                device = "green->sda1,blue->sda2",
                properties = { subtype = "kernel" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("rawfile", match.is_present({ device = "/dev/sda2" }))

        assert.spy(spy_set_bootenv).was_called_with("kernelfile", "C:BOOT1:vmlinuz")
        assert.spy(spy_set_bootenv).was_called_with("ustate", "1")
        assert.spy(spy_set_bootenv).was_called_with("kernelparams", "root=/dev/blue " .. cmdline)

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("EFI Boot Guard: Dashed filenames. #ebg", function()
        sandbox.configuration = string.dedent([[
            [kernel]
            chainhandler=rawfile

            [kernel.selector]
            method=cmdline_rr
            key=root

            [kernel.bootenv]
            kernelfile=C:BOOT${rrindex}:vmlinuz-linux
            kernelparams=root=${rrdevice} ${cmdline_root}
        ]])

        local cmdline = "rw rootwait initrd=initrd.img"
        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/sda1 " .. cmdline },
            { "/dev/sd", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                filename = "vmlinuz-linux",
                device = "sda1,sda2",
                properties = { subtype = "kernel" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("rawfile", match.is_present({ device = "/dev/sda2" }))

        assert.spy(spy_set_bootenv).was_called_with("kernelfile", "C:BOOT1:vmlinuz-linux")
        assert.spy(spy_set_bootenv).was_called_with("kernelparams", "root=/dev/sda2 " .. cmdline)

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("EFI Boot Guard: CIP Core - Kernel. #ebg #cip-core", function()
        sandbox.configuration = string.dedent([[
            [kernel]
            chainhandler=rawfile

            [kernel.selector]
            method=bootenv_rrmap
            key=kernelfile

            [kernel.bootenv]
            kernelfile=C:BOOT${rrindex}:linux.efi
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/disk/by-partlabel/BOOT0", true },
            { "/dev/disk/by-partlabel/BOOT1", true },
        })
        local bootenvstub = stubs.swupdate_get_bootenv({
            { "kernelfile", "C:BOOT0:linux.efi" },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                filename = "linux.efi",
                device = "C:BOOT0:linux.efi->BOOT0,C:BOOT1:linux.efi->BOOT1",
                properties = { subtype = "kernel" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("rawfile", match.is_present({ device = "/dev/disk/by-partlabel/BOOT1" }))
        assert.spy(spy_set_bootenv).was_called_with("kernelfile", "C:BOOT1:linux.efi")
        bootenvstub:revert()

        bootenvstub = stubs.swupdate_get_bootenv({
            { "kernelfile", "C:BOOT1:linux.efi" },
        })
        assert.is_equal(
            sandbox.handler_roundrobin({
                filename = "linux.efi",
                device = "C:BOOT0:linux.efi->BOOT0,C:BOOT1:linux.efi->BOOT1",
                properties = { subtype = "kernel" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("rawfile", match.is_present({ device = "/dev/disk/by-partlabel/BOOT0" }))
        assert.spy(spy_set_bootenv).was_called_with("kernelfile", "C:BOOT0:linux.efi")
        bootenvstub:revert()

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("EFI Boot Guard: CIP Core - Image. #ebg #cip-core", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=bootenv_rrmap
            key=kernelfile
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/disk/by-partlabel/BOOT0", true },
            { "/dev/disk/by-partlabel/BOOT1", true },
            { "/dev/disk/by-partuuid/42396f05-33de-4080-b764-6f8a46d5f205", true },
            { "/dev/disk/by-partuuid/35b7311d-5a83-47f8-b2d9-4a3166973fad", true },
        })
        local bootenvstub = stubs.swupdate_get_bootenv({
            { "kernelfile", "C:BOOT0:linux.efi" },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                filename = "image-under-test.squashfs",
                device = "C:BOOT0:linux.efi->42396f05-33de-4080-b764-6f8a46d5f205,C:BOOT1:linux.efi->35b7311d-5a83-47f8-b2d9-4a3166973fad",
                properties = { subtype = "image" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("raw", match.is_present({ device = "/dev/disk/by-partuuid/35b7311d-5a83-47f8-b2d9-4a3166973fad" }))
        bootenvstub:revert()

        bootenvstub = stubs.swupdate_get_bootenv({
            { "kernelfile", "C:BOOT1:linux.efi" },
        })
        assert.is_equal(
            sandbox.handler_roundrobin({
                filename = "image-under-test.squashfs",
                device = "C:BOOT0:linux.efi->42396f05-33de-4080-b764-6f8a46d5f205,C:BOOT1:linux.efi->35b7311d-5a83-47f8-b2d9-4a3166973fad",
                properties = { subtype = "image" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("raw", match.is_present({ device = "/dev/disk/by-partuuid/42396f05-33de-4080-b764-6f8a46d5f205" }))
        bootenvstub:revert()

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("EFI Boot Guard: CIP Core - rdiff. #ebg #cip-core #rdiff", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=rdiff_image

            [image.selector]
            method=bootenv_rrmap
            key=kernelfile

            [kernel]
            chainhandler=rawfile

            [kernel.selector]
            method=bootenv_rrmap
            key=kernelfile

            [kernel.bootenv]
            kernelfile=C:BOOT${rrindex}:linux.efi            
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/dev/disk/by-partlabel/BOOT0", true },
            { "/dev/disk/by-partlabel/BOOT1", true },
            { "/dev/disk/by-partuuid/42396f05-33de-4080-b764-boot0", true },
            { "/dev/disk/by-partuuid/35b7311d-5a83-47f8-b2d9-boot1", true },
        })
        local bootenvstub = stubs.swupdate_get_bootenv({
            { "kernelfile", "C:BOOT0:linux.efi" },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        -- "green->/dev/sda1,blue->/dev/sda2",
        assert.is_equal(
            sandbox.handler_roundrobin({
                filename = "update.delta.gz",
                device = "C:BOOT0:linux.efi->42396f05-33de-4080-b764-boot0,C:BOOT1:linux.efi->35b7311d-5a83-47f8-b2d9-boot1",
                type = "roundrobin",
                compressed = "zlib",
                properties = {
                    subtype = "image",
                    chainhandler = "rdiff_image",
                },
            }),
            0
        )
        assert.stub(callstub).was_called_with(
            "rdiff_image",
            match.is_present({
                device = "/dev/disk/by-partuuid/35b7311d-5a83-47f8-b2d9-boot1",
                rdiffbase = "/dev/disk/by-partuuid/42396f05-33de-4080-b764-boot0",
            })
        )

        assert.is_equal(
            sandbox.handler_roundrobin({
                filename = "linux.efi",
                device = "C:BOOT0:linux.efi->BOOT0,C:BOOT1:linux.efi->BOOT1",
                properties = { subtype = "kernel" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("rawfile", match.is_present({ device = "/dev/disk/by-partlabel/BOOT1" }))
        assert.spy(spy_set_bootenv).was_called_with("kernelfile", "C:BOOT1:linux.efi")

        bootenvstub:revert()
        bootenvstub = stubs.swupdate_get_bootenv({
            { "kernelfile", "C:BOOT1:linux.efi" },
        })

        assert.is_equal(
            sandbox.handler_roundrobin({
                filename = "update.delta.gz",
                device = "C:BOOT0:linux.efi->42396f05-33de-4080-b764-boot0,C:BOOT1:linux.efi->35b7311d-5a83-47f8-b2d9-boot1",
                type = "roundrobin",
                compressed = "zlib",
                properties = {
                    subtype = "image",
                    chainhandler = "rdiff_image",
                },
            }),
            0
        )
        assert.stub(callstub).was_called_with(
            "rdiff_image",
            match.is_present({
                device = "/dev/disk/by-partuuid/35b7311d-5a83-47f8-b2d9-boot1",
                rdiffbase = "/dev/disk/by-partuuid/42396f05-33de-4080-b764-boot0",
            })
        )

        assert.is_equal(
            sandbox.handler_roundrobin({
                filename = "linux.efi",
                device = "C:BOOT0:linux.efi->BOOT0,C:BOOT1:linux.efi->BOOT1",
                properties = { subtype = "kernel" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("rawfile", match.is_present({ device = "/dev/disk/by-partlabel/BOOT1" }))
        assert.spy(spy_set_bootenv).was_called_with("kernelfile", "C:BOOT1:linux.efi")

        iostub:revert()
        callstub:revert()
    end)
end)

-- =============================================================================
-- == Pre/Post Functions Tests
-- =============================================================================
busted.describe("Pre/Post Functions:", function()
    busted.before_each(function()
        swupdate = require("swupdate")
        sandbox = { swupdate = swupdate }
        setmetatable(sandbox, { __index = _G })
        spy_info = spy.on(swupdate, "info")
        spy_error = spy.on(swupdate, "error")
    end)
    busted.after_each(function()
        spy_info:revert()
        spy_error:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() success - not_match in file. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", 'IMAGE_UUID="92fd6315-a44d-49a3-9f29-ec16eb3606b6"' },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "/etc/os-release@not_match@IMAGE_UUID=13c6aa1c-3c11-4416-8924-40c23d32a8d2",
                    subtype = "image",
                },
            }),
            0
        )
        assert.stub(callstub).was_called_with(
            "raw",
            match.is_present({
                device = "/dev/blue",
                configfilecheck = "/etc/os-release@not_match@IMAGE_UUID=13c6aa1c-3c11-4416-8924-40c23d32a8d2",
            })
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() success - match in file. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", 'IMAGE_UUID="92fd6315-a44d-49a3-9f29-ec16eb3606b6"' },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "/etc/os-release@match@IMAGE_UUID=92fd6315-a44d-49a3-9f29-ec16eb3606b6",
                    subtype = "image",
                },
            }),
            0
        )
        assert.stub(callstub).was_called_with(
            "raw",
            match.is_present({
                device = "/dev/blue",
                configfilecheck = "/etc/os-release@match@IMAGE_UUID=92fd6315-a44d-49a3-9f29-ec16eb3606b6",
            })
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() failed - should match. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", 'IMAGE_UUID="92fd6315-a44d-49a3-9f29-ec16eb3606b6"' },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "/etc/os-release@match@IMAGE_UUID=13c6aa1c-3c11-4416-8924-40c23d32a8d2",
                    subtype = "image",
                },
            }),
            1
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() failed - not_match - values are identical. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", 'IMAGE_UUID="92fd6315-a44d-49a3-9f29-ec16eb3606b6"' },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "/etc/os-release@not_match@IMAGE_UUID=92fd6315-a44d-49a3-9f29-ec16eb3606b6",
                    subtype = "image",
                },
            }),
            1
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() failed - file not found. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", false },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "/etc/os-release@not_match@IMAGE_UUID=92fd6315-a44d-49a3-9f29-ec16eb3606b6",
                    subtype = "image",
                },
            }),
            1
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() failed - key not found. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", "DUMMY=92fd6315-a44d-49a3-9f29-ec16eb3606b6" },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "/etc/os-release@not_match@IMAGE_UUID=92fd6315-a44d-49a3-9f29-ec16eb3606b6",
                    subtype = "image",
                },
            }),
            1
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() failed - no key=value in parameter. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", 'IMAGE_UUID="92fd6315-a44d-49a3-9f29-ec16eb3606b6"' },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "/etc/os-release@=",
                    subtype = "image",
                },
            }),
            1
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() failed - no filename in parameter. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", 'IMAGE_UUID="92fd6315-a44d-49a3-9f29-ec16eb3606b6"' },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "@IMAGE_UUID=92fd6315-a44d-49a3-9f29-ec16eb3606b6",
                    subtype = "image",
                },
            }),
            1
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() failed - no value in parameter. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", 'IMAGE_UUID="92fd6315-a44d-49a3-9f29-ec16eb3606b6"' },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "etc/os-release@IMAGE_UUID=",
                    subtype = "image",
                },
            }),
            1
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() failed - missing match. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", 'IMAGE_UUID="92fd6315-a44d-49a3-9f29-ec16eb3606b6"' },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "/etc/os-release@@IMAGE_UUID=92fd6315-a44d-49a3-9f29-ec16eb3606b6",
                    subtype = "image",
                },
            }),
            1
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("configfilecheck() failed - match invalid value. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/etc/os%-release", 'IMAGE_UUID="92fd6315-a44d-49a3-9f29-ec16eb3606b6"' },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {
                    configfilecheck = "/etc/os-release@abcd@IMAGE_UUID=92fd6315-a44d-49a3-9f29-ec16eb3606b6",
                    subtype = "image",
                },
            }),
            1
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("mmclock() success. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/sys/block/mmcblk2boot0/force_ro", true },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = { mmclock = "mmcblk2boot0", subtype = "image" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("raw", match.is_present({ device = "/dev/blue", mmclock = "mmcblk2boot0" }))
        assert.spy(spy_info).was_called_with(match.is_string(), match.is_string(), "Unlocked", "mmcblk2boot0")
        assert.spy(spy_info).was_called_with(match.is_string(), match.is_string(), "Locked", "mmcblk2boot0")
        assert.are_same(journal, { [1] = 0, [2] = 1 })

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Pre/Post-function handler. #prepost", function()
        local chunk = load_testee(sandbox)
        chunk()

        assert.is_equal(sandbox.execute_prepost(sandbox.locals.WHEN.PRE, {}), true)
        assert.is_equal(
            sandbox.execute_prepost(
                sandbox.locals.WHEN.PRE,
                { properties = { nonexisting = "dummyparam", subtype = "image" } }
            ),
            true
        )
        sandbox.pp_fail = function()
            return false
        end
        if _VERSION == "Lua 5.1" then
            _G.pp_fail = sandbox.pp_fail
        end
        assert.is_equal(
            sandbox.execute_prepost(
                sandbox.locals.WHEN.PRE,
                { properties = { fail = "dummyparam", subtype = "image" } }
            ),
            false
        )
        assert.spy(spy_error).was_called_with(match.is_string(), match.is_string(), sandbox.locals.WHEN.PRE, "fail")
        if _VERSION == "Lua 5.1" then
            _G.pp_fail = nil
        end
    end)

    -- -----------------------------------------------------------------------------
    busted.test("mmclock() failure. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/sys/block/mmcblk2boot0/force_ro", false },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = { mmclock = "mmcblk2boot0", subtype = "image" },
            }),
            1
        )
        assert.spy(spy_error).was_called_with(match.is_string(), match.is_string(), "/sys/block/mmcblk2boot0/force_ro")

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Function not found. #prepost", function()
        sandbox.configuration = configurations.raw_cmdline_rr

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = { undeffunc = "dummyparam", subtype = "image" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("raw", match.is_present({ device = "/dev/blue", undeffunc = "dummyparam" }))
        assert.are_same(journal, {})

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("mmclock() is called on chain-handler failure. #prepost", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/sys/block/mmcblk2boot0/force_ro", true },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler(-1)
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = { mmclock = "mmcblk2boot0", subtype = "image" },
            }),
            1
        )
        assert
            .stub(callstub)
            .was_called_with("raw", match.is_present({ device = "/dev/blue", mmclock = "mmcblk2boot0" }))
        assert.are_same(journal, { [1] = 0, [2] = 1 })

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Pre-function failure. #prepost", function()
        sandbox.pp_fail = function(_, _)
            return false
        end
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=fail

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = { fail = "mmcblk2boot0", subtype = "image" },
            }),
            1
        )

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Post-function failure. #prepost", function()
        sandbox.pp_fail = function(when, _)
            if when == sandbox.locals.WHEN.POST then
                return nil
            end
            return true
        end
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local journal = {}
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/dev/blue", true },
            { "/dev/green", true },
        }, journal)
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = { fail = "mmcblk2boot0", subtype = "image" },
            }),
            1
        )

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)
end)

-- =============================================================================
-- == Fixups Tests
-- =============================================================================
busted.describe("Handler Fixups:", function()
    busted.before_each(function()
        swupdate = require("swupdate")
        sandbox = { swupdate = swupdate }
        setmetatable(sandbox, { __index = _G })
        spy_error = spy.on(swupdate, "error")
    end)
    busted.after_each(function()
        spy_error:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("rdiffbase= property present (not in device= mapping). #fixups #rdiff", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=rdiff_image

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = { rdiffbase = "dummy", subtype = "image" },
            }),
            1
        )

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("rdiff, root=/dev/green. #fixups #rdiff", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=rdiff_image

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/dev/sd", true },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green->/dev/sda1,blue->/dev/sda2",
                properties = { subtype = "image" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("rdiff_image", match.is_present({ device = "/dev/sda2", rdiffbase = "/dev/blue", subtype = "image" }))

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "green->sda1,blue->sda2", properties = { subtype = "image" } }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("rdiff_image", match.is_present({ device = "/dev/sda2", rdiffbase = "/dev/blue", subtype = "image" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("rdiff, root=/dev/blue. #fixups #rdiff", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=rdiff_image

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/blue rw rootwait initrd=initrd.img" },
            { "/dev/sd", true },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green->/dev/sda1,blue->/dev/sda2",
                properties = { subtype = "image" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("rdiff_image", match.is_present({ device = "/dev/sda1", rdiffbase = "/dev/green", subtype = "image" }))

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "green->sda1,blue->sda2", properties = { subtype = "image" } }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("rdiff_image", match.is_present({ device = "/dev/sda1", rdiffbase = "/dev/green", subtype = "image" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("delta source= property present (not in device= mapping). #fixups #delta", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=delta

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = { source = "dummy", subtype = "image" },
            }),
            1
        )

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("delta url= url property can't be empty. #fixups #delta", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=delta

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green,blue",
                properties = {  subtype = "image" },
            }),
            1
        )

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("delta, root=/dev/green. #fixups #delta", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=delta

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw rootwait initrd=initrd.img" },
            { "/dev/sd", true },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green->/dev/sda1,blue->/dev/sda2",
                properties = { subtype = "image", url = "url" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("delta", match.is_present({ device = "/dev/sda2", source = "/dev/blue", subtype = "image", url = "url", chain = "raw" }))

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "green->sda1,blue->sda2", properties = { subtype = "image", url = "url" } }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("delta", match.is_present({ device = "/dev/sda2", source = "/dev/blue", subtype = "image", url = "url", chain = "raw" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("delta, root=/dev/blue. #fixups #delta", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=delta

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/blue rw rootwait initrd=initrd.img" },
            { "/dev/sd", true },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "green->/dev/sda1,blue->/dev/sda2",
                properties = { subtype = "image", url = "url" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("delta", match.is_present({ device = "/dev/sda1", source = "/dev/green", subtype = "image", url = "url", chain = "raw" }))

        assert.is_equal(
            sandbox.handler_roundrobin({ device = "green->sda1,blue->sda2", properties = { subtype = "image", url = "url"} }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("delta", match.is_present({ device = "/dev/sda1", source = "/dev/green", subtype = "image", url = "url", chain = "raw" }))

        iostub:revert()
        callstub:revert()
    end)
end)

-- =============================================================================
-- == PLAIN/UBI/GPT Partition Tests
-- =============================================================================
busted.describe("PLAIN/UBI/GPT Partition Tests:", function()
    busted.before_each(function()
        swupdate = require("swupdate")
        sandbox = { swupdate = swupdate }
        setmetatable(sandbox, { __index = _G })
    end)
    busted.after_each(function() end)

    -- -----------------------------------------------------------------------------
    busted.test("GPT. #partitions", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            {
                "/proc/cmdline",
                "root=PARTUUID=fedcba98-7654-3210-cafe-5e0710000001 rw console=tty0 console=ttyS0,115200 rootwait initrd=initrd.img",
            },
            { "^/dev/disk/by%-partuuid/%x%x%x%x%x%x%x%x%-", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "fedcba98-7654-3210-cafe-5e0710000001,fedcba98-7654-3210-cafe-5e0710000002",
                properties = { subtype = "image" },
            }),
            0
        )
        assert
            .stub(callstub)
            .was_called_with("raw", match.is_present({ device = "/dev/disk/by-partuuid/fedcba98-7654-3210-cafe-5e0710000002" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("Plain BIOS. #partitions", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=raw

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=/dev/green rw console=tty0 console=ttyS0,115200 rootwait initrd=initrd.img" },
            { "/dev/blue", true },
            { "/dev/green", true },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()

        assert.is_equal(sandbox.handler_roundrobin({ device = "green,blue", properties = { subtype = "image" } }), 0)
        assert.stub(callstub).was_called_with("raw", match.is_present({ device = "/dev/blue" }))

        iostub:revert()
        callstub:revert()
    end)

    -- -----------------------------------------------------------------------------
    busted.test("UBI. #partitions", function()
        sandbox.configuration = string.dedent([[
            [image]
            chainhandler=ubivol

            [image.selector]
            method=cmdline_rr
            key=root
        ]])

        local chunk = load_testee(sandbox)
        local iostub = stubs.io_open({
            { "/proc/cmdline", "root=ubi0:rootfs_blue rw rootfstype=ubifs ubi.mtd=0" },
        })
        local callstub = stubs.swupdate_call_handler()
        chunk()
        local pp_injected_funcs = inject_pp_funcs(sandbox)

        assert.is_equal(
            sandbox.handler_roundrobin({
                device = "ubi0:rootfs_blue,ubi0:rootfs_green",
                properties = { subtype = "image" },
            }),
            0
        )
        assert.stub(callstub).was_called_with("ubivol", match.is_present({ device = "ubi0:rootfs_green" }))

        eject_pp_funcs(pp_injected_funcs)
        iostub:revert()
        callstub:revert()
    end)
end)
