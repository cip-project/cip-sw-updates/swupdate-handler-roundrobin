--[[

    SWUpdate Round Robin Handler.

    Copyright (C) 2021-2023, Siemens AG
    Author: Christian Storm <christian.storm@siemens.com>

    SPDX-License-Identifier: GPL-2.0-or-later

--]]

--luacheck: no max line length
--luacheck: no global
--luacheck: no unused args
---@diagnostic disable: undefined-global
---@diagnostic disable: unused-local

--[[ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ]]
--[[  Compiled- / Built-in Configuration                                       ]]
--[[ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ]]

-- local configuration = [[ ... ]]

--[[ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ]]
--[[  Round Robin Handler Implementation                                       ]]
--[[ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ]]

--- `require()` the SWUpdate Lua module if not loaded.
if not package.loaded["swupdate"] then
    swupdate = require("swupdate")
end

--- Table holding the Round Robin Handler configuration.
--
--- @class rrconfig
--- @field file     string  External configuration file to parse.
--- @field builtin  string  Compiled-in configuration "file" contents.
--- @field values   table   Parsed configuration file settings (cache).
local rrconfig = {
    -- Default configuration file, tried if no built-in config is defined.
    file = "/etc/swupdate.handler.ini",
    builtin = configuration or "",
    values = {},
}

--- Partition type enum-alike and reverse lookup table.
--
-- Note: Keep in sync with swupdate.ROOT_DEVICE Table, 1-indexed.
--       See selector.source.getroot() function.
--
--- @enum ROOT_DEVICE_TYPE
local ROOT_DEVICE_TYPE = {
    PATH = 1,
    [1] = "/dev/",
    UUID = 2,
    [2] = "UUID=",
    PARTUUID = 3,
    [3] = "PARTUUID=",
    PARTLABEL = 4,
    [4] = "PARTLABEL=",
    UBI = 5,
    [5] = "",
}

--- Pre/post-handler execution point enum-alike.
--
--- @enum WHEN
local WHEN = {
    PRE = "Pre",
    POST = "Post",
    FAIL = "Fail",
}

--- Name of this handler used as logging output prefix.
local logprefix = "Round Robin Handler"

--- @class rrtargets
--- @field map table             # device remap or identity map.
--
-- Round Robin Handler targets Table: A Table with values
-- indexed by number, i.e. t[<number>] = <value>, plus
-- reverse lookup, i.e. t[<value>] = <number>.
--
-- If there's a mapping specified in the `device=` field, e.g.,
-- `green->sda1,blue->sda2`, it is stored in the `map` subtable.
-- Otherwise the `map` subtable is an identity map.
-- The `map` subtable is used by the `rrmap` algorithm.
--
-- Example: The Table for an sw-description's `device=` field
-- value "/dev/sda1,/dev/sda2" is {
--    [1]      = "sda1", [2]      = "sda2",
--    ["sda1"] = 1,      ["sda2"] = 2,
--    ["map"]  = {
--       ["sda2"] = "sda2",
--       ["sda1"] = "sda1"
--    }
-- }
--
-- @see image_device_parse() that returns an `rrtargets` Table
-- parsed from sw-description's `device=` field string.
--
--- @type  table<number,string>  # values indexed by number.
--- @type  table<string,number>  # number indexed by values.

--- @class rrtarget
--
-- The Round Robin Handler target as indexes into
-- the `rrtargets` Table.
--
-- Example: { rrindex=2, rrtarget="sda2" }
--
-- See `selector.algorithm[...]` that return an `rrtarget`
-- Table holding the Round Robin calculation result.
--
--- @field rrindex   number  Index into `rrtargets` Table.
--- @field rrtarget  string  Index into `rrtargets` Table.

--- @class overrides
--
-- Overrides to some sanity checks.
-- Currently required for `selector.fixed()` only.
--
--- @field mintargets  number   Number of targets, defaults to 2 for A/B deployments.
--- @field ignorelist  boolean  Whether to ignore the list of targets altogether, defaults to false.

--- Parse and process sw-description's `device=` field string.
--
--- @param  csv        string    A comma separated string (i.e. sw-description's `device=` field).
--- @param  callback?  function  Function to post-process the parsed CSV item [optional].
--- @return rrtargets            # Round Robin Handler targets Table, or, in case of error, {}.
local function image_device_parse(csv, callback)
    if not csv or csv == "" then
        return {}
    end
    callback = callback or function(item)
        return item
    end
    --- @type table
    local dict = { map = {} }
    for item in csv:gmatch("([^,]+)") do
        local pattern = "(%g+)%->(%g+)"
        if _VERSION == "Lua 5.1" and type(jit) ~= "table" then
            pattern = "([%w/%-_:%.]+)%->([%w/%-_:%.]+)"
        end
        local device_node, map = item:match(pattern)
        device_node = callback(device_node and device_node or item)
        map = callback(map and map or device_node)
        dict[#dict + 1] = device_node
        dict[device_node] = #dict
        dict.map[device_node] = map
        dict.map[map] = device_node
    end
    local mapcount = 0
    for _ in pairs(dict.map) do
        mapcount = mapcount + 1
    end
    if mapcount % #dict ~= 0 then
        swupdate.warn(
            "%s: Mismatch parsing CSV string '%s', expected %d values, got %d.",
            logprefix,
            csv,
            mapcount + (mapcount % #dict),
            mapcount
        )
    end
    return dict
end

--- Strip (optional) prefix and leading paths from a device path / UUID / Label.
--
--- @param  device_path  string  Device string to strip, e.g., "root=/dev/sda1", "root=PARTUUID=<UUID>", ...
--- @param  prefix?      string  Prefix to strip, e.g., "root=".
--- @return string | nil         # `device_path` (same as input) or, in case of error, nil.
--- @return string | nil         # `device_path` prefix- and leading qualifier/path-stripped or, in case of error, nil.
local function strip_dev_path(device_path, prefix)
    local result_path = device_path
    if not result_path then
        return
    end
    if prefix and result_path:find(prefix, 1, true) == 1 then
        result_path = result_path:sub(1 + #prefix)
    end
    -- See the Kernel's init/do_mounts.c :: name_to_dev_t() for reference.
    for _, rdt in ipairs(ROOT_DEVICE_TYPE) do
        --- @cast rdt string
        if #rdt > 0 and result_path:find(rdt, 1, true) == 1 then
            return device_path, result_path:sub(1 + #rdt)
        end
    end
    return device_path, result_path
end

--- Find the fully qualified /dev/... path to a device node.
--
--- @param  device_node  string         Device node name, e.g., "sda1".
--- @param  quiet        boolean | nil  Don't error if device not found.
--- @return boolean                     # True, or, in case of error, false.
--- @return string | nil                # The full /dev/... device path, or nil in case of error.
--- @return ROOT_DEVICE_TYPE | nil      # The partition type (numeric), or nil in case of error.
local function get_dev_path(device_node, quiet)
    local function test_dev_path(device_path, partition_type)
        local node = io.open(device_path, "rb")
        if node then
            node:close()
            return device_path, partition_type
        end
        return nil, nil
    end

    if device_node:match("ubi%d+:%S+") then
        return true, device_node, ROOT_DEVICE_TYPE.UBI
    end
    for _, loc in ipairs({
        { path = "/dev/%s", parttype = ROOT_DEVICE_TYPE.PATH },
        { path = "/dev/mapper/%s", parttype = ROOT_DEVICE_TYPE.PATH },
        { path = "/dev/disk/by-uuid/%s", parttype = ROOT_DEVICE_TYPE.UUID },
        { path = "/dev/disk/by-partuuid/%s", parttype = ROOT_DEVICE_TYPE.PARTUUID },
        { path = "/dev/disk/by-partlabel/%s", parttype = ROOT_DEVICE_TYPE.PARTLABEL },
    }) do
        local device_path, partition_type = test_dev_path(string.format(loc.path, device_node), loc.parttype)
        if device_path then
            return true, device_path, partition_type
        end
    end
    if not quiet then
        swupdate.error("%s: Cannot find/open target device node '%s' in /dev/", logprefix, device_node)
    end
    return false, nil, nil
end

--- Execute pre/post-handler function(s) as specified in sw-description's properties Table.
--
--- @param  when   WHEN      One of `WHEN`'s values.
--- @param  image  img_type  The `struct img_type {...}` Lua Table equivalent.
--- @return boolean          # True, or, in case of pre/post-handler function error, false.
function execute_prepost(when, image)
    if not image.properties then
        return true
    end
    if _VERSION == "Lua 5.1" then
        _ENV = _G
    end
    for func, funcparam in pairs(image.properties) do
        local funcname = string.format("pp_%s", func)
        if _ENV[funcname] and type(_ENV[funcname]) == "function" then
            swupdate.info("%s: Executing on%s function '%s()'.", logprefix, when, func)
            if not _ENV[funcname](when, funcparam, image) then
                swupdate.error("%s: on%s function '%s()' failed.", logprefix, when, func)
                return false
            end
        end
    end
    return true
end

--- Load & Parse INI-style configuration.
--
-- Loaded and parsed configuration is stored as Table in
-- `rrconfig.values` and returned.
--
--- @param  cfgtbl  rrconfig  Round Robin Handler configuration Table.
--- @return table | nil       # rrconfig.values, or, in case of error, nil.
local function load_config(cfgtbl)
    -- Return config right away if it has been parsed, i.e., cached.
    if next(cfgtbl.values) ~= nil then
        return cfgtbl.values
    end

    -- Get INI-style string.
    local cfgstr = cfgtbl.builtin or ""
    if #cfgstr == 0 then
        cfgtbl.file = cfgtbl.file and cfgtbl.file or "/dev/null"
        swupdate.trace("%s: No compiled-in config found, trying %s", logprefix, cfgtbl.file)
        local file = io.open(cfgtbl.file, "r")
        if not file then
            swupdate.error("%s: Cannot open config file %s", logprefix, cfgtbl.file)
            return nil
        end
        cfgstr = file:read("*a")
        file:close()
    end
    if (cfgstr or ""):sub(-1) ~= "\n" then
        cfgstr = cfgstr .. "\n"
    end

    -- Parse INI-style string's content into Lua Table.
    local cfgsec, key, value
    for line in (cfgstr or ""):gmatch("(.-)\n") do
        if line:match("^%[([%w%p]+)%][%s]*") then
            line = line:match("^%[([%w%p]+)%][%s]*")
            cfgsec = cfgtbl.values
            for subsec in line:gmatch("(%w+)") do
                cfgsec[subsec] = cfgsec[subsec] and cfgsec[subsec] or {}
                cfgsec = cfgsec[subsec]
            end
        elseif cfgsec then
            key, value = line:match("^([%w%p]-)=(.*)$")
            if key and value then
                value = tostring(value):match("^%s*(.-)%s*$")
                if tonumber(value) then
                    value = tonumber(value)
                end
                if value == "true" then
                    value = true
                end
                if value == "false" then
                    value = false
                end
                cfgsec[key] = value
            else
                if not line:match("^$") and not line:match("^#") then
                    swupdate.warn("%s: Syntax error, skipping '%s'", logprefix, line)
                end
            end
        else
            swupdate.error("%s: Syntax error. No [section] encountered.", logprefix)
            return nil
        end
    end

    return cfgtbl.values
end

--- Round Robin Selectors, Sources, Algorithms, and Callbacks Table.
--- @type table
local selector = {
    callbacks = {
        cmdline = {},
        bootenv = {},
        getroot = {},
    },
    algorithm = {},
    source = {},
    cache = {},
}

--- Algorithms.
--
-- The `selector.algorithm[...]` functions do the actual
-- Round Robin calculation, based on information gathered
-- via the `selector.source[...]` functions.
--
--- @param  rrcurrent  string     Current device node to run algorithm with, e.g., "sda1".
--- @param  rrtargets  rrtargets  Round Robin targets Table.
--- @return rrtarget              # The calculated Round Robin Handler target Table.
function selector.algorithm.rr(rrcurrent, rrtargets)
    local index = rrtargets[rrcurrent] and rrtargets[rrcurrent] % #rrtargets + 1
    if not index then
        return {}
    end
    return { rrindex = index, rrtarget = rrtargets[index] }
end

selector.algorithm.rrmap = selector.algorithm.rr

function selector.algorithm.id(rrcurrent, rrtargets)
    local index = rrtargets[rrcurrent] and rrtargets[rrcurrent]
    if not index then
        return {}
    end
    return { rrindex = index, rrtarget = rrtargets[index] }
end

--- Sources.
--
-- The `selector.source[...]` functions do the information retrieval on which
-- the Round Robin calculation (see `selector.algorithm[...]`) is performed.
--
-- Example return value: "sda1", { rrindex = 2, rrtarget = "sda2" }
--
--- @param  key        string     Key to lookup in cmdline / bootenv / ..., e.g., "root".
--- @param  rrtargets  rrtargets  Round Robin targets Table.
--- @param  algorithm  function   One of `selector.algorithm[...]`'s functions.
--- @return string | nil          # `key`'s value, or, in case of error, nil.
--- @return rrtarget              # The calculated Round Robin Handler target Table.
function selector.source.bootenv(key, rrtargets, algorithm)
    -- luacheck: ignore fvalue
    local fvalue, value = selector.callbacks.bootenv[key](swupdate.get_bootenv(key), key)
    if not value then
        swupdate.error("%s: Error getting key '%s' from bootloader environment.", logprefix, key)
        return nil, {}
    end
    return value, algorithm(value, rrtargets)
end

function selector.source.cmdline(key, rrtargets, algorithm)
    if not selector.cache.cmdline then
        local file = io.open("/proc/cmdline", "r")
        if not file then
            swupdate.error("%s: Cannot open %s.", logprefix, "/proc/cmdline")
            return nil, {}
        end
        selector.cache.cmdline = file:read("*l")
        file:close()
    end

    local cachekeybase = string.format("cmdline_%s", key)
    if selector.cache[cachekeybase] then
        return selector.cache[cachekeybase .. "_value"], algorithm(selector.cache[cachekeybase .. "_value"], rrtargets)
    end

    local pattern = string.format("%%f[%%g]%s=%%g+", key)
    if _VERSION == "Lua 5.1" and type(jit) ~= "table" then
        pattern = string.format("%%f[%%w%%-_:]%s=[%%w/=%%-_:]+", key)
    end
    local fvalue, value = selector.callbacks.cmdline[key](selector.cache.cmdline:match(pattern), key)
    if not value then
        swupdate.error("%s: Error parsing %s's value from /proc/cmdline.", logprefix, key)
        return nil, {}
    end

    selector.cache[cachekeybase] =
        string.match(string.gsub(selector.cache.cmdline, fvalue:gsub("%-", "%%-"), ""):gsub("%s+", " "), "^%s*(.-)%s*$")
    selector.cache[cachekeybase .. "_token"] = fvalue
    selector.cache[cachekeybase .. "_value"] = value

    swupdate.debug("%s: Parsed %s=%s from /proc/cmdline", logprefix, key, value)

    return value, algorithm(value, rrtargets)
end

function selector.source.getroot(key, rrtargets, algorithm)
    root = swupdate.getroot() or {}
    if not root.value then
        swupdate.error("%s: No return values from SWUpdate's getroot().", logprefix)
        return nil, {}
    end
    swupdate.debug("%s: Got %s from SWUpdate's getroot()", logprefix, root.value)
    local _, value = strip_dev_path(root.value, ROOT_DEVICE_TYPE[root.type + 1])
    return value, algorithm(value, rrtargets)
end

--- Sources Processing Callbacks.
--
-- Filter/Fixup/Processings for particular `selector.source[...]` results.
--
--- @param  value  string  Returned value from a source query for `key`, e.g., "root=/dev/sda1".
--- @param  key    string  Lookup key used to query the source, e.g., "root".
--- @return string | nil   # Normalized `key=value` pair or, in case of error, nil.
--- @return string | nil   # Normalized `key` or, in case of error, nil,
function selector.callbacks.bootenv.root(value, key)
    return strip_dev_path(value)
end
setmetatable(selector.callbacks.bootenv, {
    __index = function()
        return function(value, key)
            if not value then
                return nil, nil
            end
            return string.format("%s=%s", key, value), value
        end
    end,
})
function selector.callbacks.cmdline.root(value, key)
    -- Special treatment for `root=...` cmdline argument to remove the
    -- leading "root=" prefix and the leading device node paths.
    return strip_dev_path(value, key .. "=")
end
setmetatable(selector.callbacks.cmdline, {
    __index = function()
        return function(value, key)
            if not value then
                return nil, nil
            end
            return value, (value:gsub(string.format("^%s=", key), ""))
        end
    end,
})

--- Round Robin Methods.
--
-- Setup convenient callables combining Sources with Algorithms, i.e.
-- <source>_<algorithm> in the `selector` Table, for being used in the
-- configuration's `[<>.selector]` section as `method` value.
--
for _, source in ipairs({ "bootenv", "cmdline", "getroot" }) do
    for _, alg in ipairs({ "rr", "id", "rrmap" }) do
        --- @param  key        string     Key to lookup in cmdline / bootenv / ..., e.g., "root".
        --- @param  rrtargets  rrtargets  Round Robin targets Table.
        --- @return overrides             # Overrides to some sanity checks.
        --- @return string                # The current root device not the algorithm was run with, e.g., "sda1".
        --- @return rrtarget              # The calculated Round Robin Handler target Table.
        selector[string.format("%s_%s", source, alg)] = function(key, rrtargets)
            return {}, selector.source[source](key, rrtargets, selector.algorithm[alg])
        end
    end
end
function selector.fixed(key, rrtargets)
    _, value = strip_dev_path(key, "")
    return { mintargets = 1, ignorelist = true }, value, { rrindex = 1, rrtarget = value }
end

local handler_fixups = {}

--- Fixups for particular chain-callee handlers.
--
-- Some chain-callee handlers such as the `rdiff_image` and `delta` handler require
-- specific fixups in the `struct img_type {...}` Lua Table equivalent
-- passed on to them.
--
--- @param  image      img_type   The `struct img_type {...}` Lua Table equivalent.
--- @param  rrtarget   table      See Algorithms.
--- @param  rrtargets  rrtargets  Round Robin Handler targets Table.
--- @return boolean | nil         # True, or, in case of error, nil.
--- @return img_type              # The modified `struct img_type {...}` Lua Table equivalent.
function handler_fixups.delta(image, rrtarget, rrtargets)
    if image.properties.chainhandler == "rdiff_image" and image.properties.rdiffbase then
        swupdate.error("%s: rdiffbase= present in properties, use device= mapping instead.", logprefix)
        return nil, image
    end
    if image.properties.chainhandler == "delta" and image.properties.source then
        swupdate.error("%s: source= present in properties, use device= mapping instead.", logprefix)
        return nil, image
    end
    if image.properties.chainhandler == "delta" and not image.properties.url then
        swupdate.error("%s: image.properties.url can't be empty", logprefix)
        return nil, image
    end
    local res, source, _ = get_dev_path(rrtargets[rrtarget.rrindex], true)
    if not res then
        -- `rrtargets[rrtarget.rrindex]` is not an existing device, could be a "label".
        -- In this case, do a RR calculation to find the `rdiffbase`.
        local rr_source = selector.algorithm.rr(rrtargets[rrtarget.rrindex], rrtargets)
        res, source, _ = get_dev_path(rrtargets.map[rr_source.rrtarget])
        if not res then
            return nil, image
        end
    end
    if image.properties.chainhandler == "rdiff_image" then
        image.properties.rdiffbase = source
        swupdate.info("%s: Using '%s' as rdiffbase.", logprefix, image.properties.rdiffbase)
    elseif image.properties.chainhandler == "delta" then
        image.properties.chain = "raw"
        image.properties.source = source
        swupdate.info("%s: Using '%s' as source.", logprefix, image.properties.source)
    end
    return true, image
end

handler_fixups["rdiff_image"] = handler_fixups.delta

setmetatable(handler_fixups, {
    __index = function()
        return function(item)
            return true, item
        end
    end,
})

--- Round Robin Handler.
--
--- @param  image  img_type  The `struct img_type {...}` Lua Table equivalent.
--- @return number           # 0, or, in case of error, 1.
function handler_roundrobin(image)
    -- Read configuration.
    local config = load_config(rrconfig)
    if not config then
        swupdate.error("%s: Cannot read configuration.", logprefix)
        return 1
    end

    -- Check for mandatory configuration section reference in the `subtype`
    -- field of the `image.properties` Table.
    if not (image.properties or {}).subtype then
        swupdate.error("%s: 'subtype' not present in properties.", logprefix)
        return 1
    end

    -- Select configuration matching `image.properties.subtype`.
    config = config[image.properties.subtype]
    if not config then
        swupdate.error("%s [%s] configuration section absent.", logprefix, image.properties.subtype)
        return 1
    end
    if not ((config.selector or {}).method and ((config.selector or {}).value or (config.selector or {}).key)) then
        swupdate.error("%s: [%s.selector] section invalid.", logprefix, image.properties.subtype)
        return 1
    end
    if not (selector[config.selector.method] and type(selector[config.selector.method]) == "function") then
        swupdate.error(
            "%s: Selector method '%s' in [%s.selector] not implemented.",
            logprefix,
            config.selector.method,
            image.properties.subtype
        )
        return 1
    end

    -- Enrich `image.properties` Table with its configuration.
    setmetatable(image.properties, { __index = config })

    -- Check whether the chain-callee handler is available.
    if not swupdate.handler[image.properties.chainhandler] then
        swupdate.error(
            "%s: Unknown chain-callee handler '%s'.",
            logprefix,
            image.properties.chainhandler or "<unspecified>"
        )
        return 1
    end

    -- Get round robin target list.
    local rrtargets = image_device_parse(image.device, function(item)
        return (item:gsub("^/dev/", ""))
    end)

    -- Determine current option and perform round robin calculation for target.
    local key = config.selector.key and config.selector.key or config.selector.value
    local overrides, rrcurrent, rrtarget = selector[config.selector.method](key, rrtargets)

    local mintargets = overrides.mintargets and overrides.mintargets or 2
    if #rrtargets < mintargets then
        swupdate.error("%s: Specify at least %d targets in the device= property.", logprefix, mintargets)
        return 1
    end
    if not rrcurrent then
        swupdate.error("%s: Could not determine current root device.", logprefix)
        return 1
    end

    if not overrides.ignorelist and not rrtargets[rrcurrent] then
        swupdate.error(
            "%s: '%s' is not in round robin target list: %s",
            logprefix,
            rrcurrent,
            table.concat(rrtargets, " ")
        )
        return 1
    end

    -- Resolve mapping (usually maps to identity if no mapping is given in sw-description's `device=` field).
    rrtarget.rrtarget = overrides.ignorelist and rrtarget.rrtarget or rrtargets.map[rrtarget.rrtarget]

    local res
    -- Apply fixups for specific chain-callee handlers.
    res, image = handler_fixups[image.properties.chainhandler](image, rrtarget, rrtargets)
    if not res then
        return 1
    end

    -- Get the target's device path and partition type.
    res, rrtarget.rrdevice, rrtarget.rrparttype = get_dev_path(rrtarget.rrtarget)
    rrtarget.rrpartprefix = ROOT_DEVICE_TYPE[rrtarget.rrparttype]
    if not res then
        return 1
    end

    swupdate.info(
        "%s: Using '%s' as target via '%s' handler.",
        logprefix,
        rrtarget.rrdevice,
        image.properties.chainhandler
    )

    -- Execute property-defined pre-handler function(s).
    if not execute_prepost(WHEN.PRE, image) then
        return 1
    end

    -- Actually flash the partition.
    local msg
    image.type = image.properties.chainhandler
    image.device = rrtarget.rrdevice
    res, msg = swupdate.call_handler(image.properties.chainhandler, image)
    if res ~= 0 then
        execute_prepost(WHEN.FAIL, image)
        swupdate.error(
            "%s: Error chain-calling '%s' handler: %s",
            logprefix,
            image.properties.chainhandler,
            (msg or "")
        )
        return 1
    end

    -- Enrich `rrtarget` with keys from `image`, `config`, etc. for placeholder
    -- replacement in the `[<>.bootenv]` section.
    -- Note: Sub-table indexing is not supported, only non-table fields are resolved.
    setmetatable(rrtarget, {
        __index = function(_, needle)
            local function chktype(item)
                if type(item) == "string" or type(item) == "number" then
                    return item
                end
            end
            if chktype(rawget(selector.cache, needle)) then
                return rawget(selector.cache, needle)
            end
            if chktype(rawget(image.properties, needle)) then
                return rawget(image.properties, needle)
            end
            if chktype(rawget(config, needle)) then
                return rawget(config, needle)
            end
            if chktype(rawget(image, needle)) then
                return rawget(image, needle)
            end
        end,
    })

    -- Actually update the Bootloader environment, if applicable.
    rrtarget.rrindex = rrtarget.rrindex - 1
    for item, value in pairs(config.bootenv or {}) do
        value = (tostring(value):gsub("%${([%w_]+)}", rrtarget))
        if not value:match("%${([^}]-)}") then
            swupdate.info("%s: Setting bootloader environment: %s=%s", logprefix, item, value)
            swupdate.set_bootenv(item, value)
        else
            swupdate.error("%s: Error substituting bootloader environment: %s=%s.", logprefix, item, value)
            swupdate.warn("%s: Continuing anyway, check your configuration!", logprefix)
        end
    end

    -- Execute property-defined post-handler function(s).
    if not execute_prepost(WHEN.POST, image) then
        return 1
    end

    return 0
end

swupdate.register_handler(
    "roundrobin",
    handler_roundrobin,
    swupdate.HANDLER_MASK.IMAGE_HANDLER + swupdate.HANDLER_MASK.FILE_HANDLER
)

--[[ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ]]
--[[  Pre/Post-Handler Functions                                               ]]
--[[ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ]]

--- Lock/Unlock an eMMC partition.
--
-- Example for a Pre/Post-Handler Function.
--
--- @param  when    WHEN      One of `WHEN`'s values.
--- @param  device  string    The sw-description's properties field `mmclock`'s value.
--- @param  image   img_type  The `struct img_type {...}` Lua Table equivalent, after round robin calculation.
--- @return boolean           # True on success or false on error.
function pp_mmclock(when, device, image)
    local filename = string.format("/sys/block/%s/force_ro", device)
    local filehandle = io.open(filename, "wb")
    if not filehandle then
        swupdate.error("%s: Cannot open file %s", logprefix, filename)
        return false
    end
    filehandle:write(when == WHEN.PRE and 0 or 1)
    filehandle:flush()
    filehandle:close()
    swupdate.info("%s: %s MMC device %s", logprefix, when == WHEN.PRE and "Unlocked" or "Locked", device)
    return true
end

--- Check the given config file for key=value.
--
-- If the config file contains key=value return true according to the match|not_match selector.
-- The match selector defines if the given key value should match the file content or should
-- not match the file content.
--
-- The function returns false in case of one of the following errors:
--  - If the file does not exist.
--  - If the file does not contain the key.
--  - If the config parameter does not match the defined format:
--     <full qualified filename>@<match|not_match>@<key>=<value>
--  - If the config parameter does not follow the allowed patterns which are
--    POSIX fully portable filename characters (A-Z a-z 0-9 . _ -) for the fully
--    qualified filename, letters (A-Z a-z) and underscore (_) for the matcher,
--    and POSIX fully portable filename characters for key as well as for value.
--
--- @param  when      WHEN      One of `WHEN`'s values.
--- @param  config    string    The sw-description's properties field `configfilecheck`'s value.
--- @param  image     img_type  The `struct img_type {...}` Lua Table equivalent, after round robin calculation.
--- @return boolean   # True on success or false on error.
function pp_configfilecheck(when, config, image)
    if when ~= WHEN.PRE then
        return true
    end
    local llogprefix = string.format("%s/configfilecheck", logprefix)
    local filename, match, key, value = string.match(config, "([%w/_%-.]+)@([%a_]+)@([%w/_%-.]+)=([%w/_%-.]+)")
    if not filename or not key or not value or not match then
        swupdate.error(
            "%s: parameter %s does not follow the format <filename>@<match|not_match>@<key>=<value>",
            llogprefix,
            config
        )
        return false
    end
    local filehandle = io.open(filename, "rb")
    if not filehandle then
        swupdate.error("%s: Cannot open file %s", llogprefix, filename)
        return false
    end
    local filecontent = filehandle:read("*a")
    filehandle:close()
    local element = string.match(filecontent, "[\n\r]?" .. key .. "=([^\n\r]+)")
    if not element then
        swupdate.error("%s: Key %s is not in %s", llogprefix, key, filename)
        return false
    end
    local result, _ = element:gsub("[\"']", "")
    if result == value and match == "match" then
        swupdate.debug("%s: value '%s' == '%s'", llogprefix, value, result)
        return true
    end
    if result ~= value and match == "not_match" then
        swupdate.debug("%s: value '%s' ~= '%s'", llogprefix, value, result)
        return true
    end
    swupdate.error("%s: '%s=%s' !%s in '%s'.", llogprefix, key, value, match, filename)
    return false
end
